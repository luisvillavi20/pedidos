import csv
from flask import Flask, render_template, request, redirect, url_for, jsonify
from flask_socketio import SocketIO, emit
from threading import Lock
from datetime import datetime
import logging

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger('socket_logger')
thread = None
thread_lock = Lock()
CSV_PATH = "C:\\Users\\Luis Villavicencio\\.spyder-py3\\TDF\\CERCIS592023071715.temp"


app = Flask(__name__, template_folder='templates')
#app = Flask(__name__, template_folder='/usr/local/etc/nginx/TDF')
app.config['SECRET_KEY'] = 'your_secret_key'
socketio = SocketIO(app)
last_sent_id = None

def preprocess_csv_data(csv_file_path):
    # Read data from the CSV file
    data = []
    with open(csv_file_path, 'r') as csv_file:
        csv_reader = csv.reader(csv_file)
        for row in csv_reader:
            laTmp = {'ID': row[0], 'IDEQUIPO': row[1], 'ALARMIDD': row[2], 'ALARMDES': row[3],
                     'ALARMTIM': row[4], 'ALARMSEV': row[5], 'ALARMINT': row[6], 'ESTADO': row[7],
                     'CHECK': row[8], 'COMENTARIO': row[9], 'FECHACHECK': row[10]}
            data.append(laTmp)    
    sorted_data = sorted(data, key=lambda x: x['ID'])
    return data

def send_data_to_client():
    global last_sent_id
    while True:
        data = preprocess_csv_data('C:\\Users\\Luis Villavicencio\\.spyder-py3\\TDF\\CERCIS592023071715.temp')
        if last_sent_id is not None:
            data = [row for row in data if int(row['ID']) > int(last_sent_id)]
        if data:
            last_sent_id = data[-1]['ID']
            socketio.emit('new_rows', data)
        #print(data)
        #socketio.emit('update_rows', data)
        socketio.sleep(5) 


@app.route('/', methods=['GET', 'POST'])
def display_table():
    csv_file_path = 'C:\\Users\\Luis Villavicencio\\.spyder-py3\\TDF\\CERCIS592023071715.temp'
    #csv_file_path = './CERCIS592023071715.temp'
    if request.method == 'POST':
        laData = {'DATOS': [row for row in preprocess_csv_data(csv_file_path)]}
        submitted_data = request.form
        textbox_values = []
        checkbox_values = []
        fechacheck_values = []
        for row in laData['DATOS']:
            row_id = row['ID']
            if row['COMENTARIO'] != '':
                textbox_value = row['COMENTARIO']
            else:
                textbox_value = submitted_data.get('textbox_[' + row_id + ']', '')
            if row['CHECK'] != '':
                checkbox_value = row['CHECK']
            else:
                checkbox_value = 'checked' if submitted_data.get('checkbox_[' + row_id + ']') else ''
            if row['FECHACHECK'] != '':
                fechacheck_value = row['FECHACHECK']
            else:
                fechacheck_value = submitted_data.get('fechacheck_[' + row_id + ']', '')  # Get 'Fecha Check' value
            laTmp = {'CHECK': checkbox_value, 'COMENTARIO': textbox_value, 'FECHACHECK': fechacheck_value}  # Include 'FECHACHECK' in the dictionary
            row.update(laTmp)
            if row['COMENTARIO']:
                row['ESTADO'] = 'B'
        # Save the modified data to the output CSV file
        with open('C:\\Users\\Luis Villavicencio\\.spyder-py3\\TDF\\CERCIS592023071715.temp', 'w', newline='') as csv_file:
        #with open('./CERCIS592023071715.temp', 'w', newline='') as csv_file:
            campos = ['ID', 'IDEQUIPO', 'ALARMIDD', 'ALARMDES', 'ALARMTIM', 'ALARMSEV', 'ALARMINT','ESTADO', 'CHECK', 'COMENTARIO', 'FECHACHECK']  # Include 'FECHACHECK'
            csv_writer = csv.DictWriter(csv_file, fieldnames=campos)
            csv_writer.writerows(laData['DATOS'])
        return redirect('/')
    
    laData = {'DATOS': [row for row in preprocess_csv_data(csv_file_path) if row['ESTADO'] == 'A']}
    return render_template('index.html', data=laData['DATOS'], num_rows=len(laData['DATOS']),
    #return render_template('./index.html', data=laData['DATOS'], num_rows=len(laData['DATOS']),
                   textbox_names=['textbox_' + str(row['ID']) for row in laData['DATOS']],
                   checkbox_names=['checkbox_' + str(row['ID']) for row in laData['DATOS']])

@app.route('/current-date', methods=['GET'])
def current_date():
    current_date = datetime.now()
    date_str = current_date.strftime("%d/%m/%Y, %H:%M:%S")
    
    return jsonify({'current_date': date_str})

@app.route('/display_status_A', methods=['GET','POST'])
def display_status_A():
    csv_file_path = 'C:\\Users\\Luis Villavicencio\\.spyder-py3\\TDF\\CERCIS592023071715.temp'
    #csv_file_path = './CERCIS592023071715.temp'
    laData = {'DATOS': [row for row in preprocess_csv_data(csv_file_path) if row['ESTADO'] == 'A']}
    #####
    
    
    
    #####
    #return render_template('display_status_A.html', data=laData['DATOS'], num_rows=len(laData['DATOS']),
    return render_template('./display_status_A.html', data=laData['DATOS'], num_rows=len(laData['DATOS']),
                           textbox_names=['textbox_' + str(i) for i in range(len(laData['DATOS']))],
                           checkbox_names=['checkbox_' + str(i) for i in range(len(laData['DATOS']))])
                           
@app.route('/display_status_B', methods=['GET','POST'])
def display_status_B():
    csv_file_path = 'C:\\Users\\Luis Villavicencio\\.spyder-py3\\TDF\\CERCIS592023071715.temp'
    #csv_file_path = './CERCIS592023071715.temp'
    #####
    print('some post')
    if request.method == 'POST':
        print('some')
    
        laData = {'DATOS': [row for row in preprocess_csv_data(csv_file_path)]}
        submitted_data = request.form
        textbox_values = []
        checkbox_values = []
        fechacheck_values = []
        for row in laData['DATOS']:
            row_id = row['ID']
            if row['COMENTARIO'] == submitted_data.get('textbox_[' + row_id + ']', ''):
                textbox_value = row['COMENTARIO']
            else:
                textbox_value = submitted_data.get('textbox_[' + row_id + ']', '')
            if row['CHECK'] == submitted_data.get('checkbox_[' + row_id + ']'):
                checkbox_value = row['CHECK']
            else:
                checkbox_value = 'checked' if submitted_data.get('checkbox_[' + row_id + ']') else ''
            if row['FECHACHECK'] == submitted_data.get('fechacheck_[' + row_id + ']', ''):
                fechacheck_value = row['FECHACHECK']
            else:
                current_date = datetime.now()
                date_str = current_date.strftime("%d/%m/%Y, %H:%M:%S")
                fechacheck_value = date_str
            laTmp = {'CHECK': checkbox_value, 'COMENTARIO': textbox_value, 'FECHACHECK': fechacheck_value}
            row.update(laTmp)
            if row['COMENTARIO']:
                row['ESTADO'] = 'B'
        # Save the modified data to the output CSV file
        with open('C:\\Users\\Luis Villavicencio\\.spyder-py3\\TDF\\CERCIS592023071715.temp', 'w', newline='') as csv_file:
        #with open('./CERCIS592023071715.temp', 'w', newline='') as csv_file:
            campos = ['ID', 'IDEQUIPO', 'ALARMIDD', 'ALARMDES', 'ALARMTIM', 'ALARMSEV', 'ALARMINT','ESTADO', 'CHECK', 'COMENTARIO', 'FECHACHECK']  # Include 'FECHACHECK'
            csv_writer = csv.DictWriter(csv_file, fieldnames=campos)
            csv_writer.writerows(laData['DATOS'])
            return redirect('/')
    ##### 
    laData = {'DATOS': [row for row in preprocess_csv_data(csv_file_path) if row['ESTADO'] == 'B']}
    #return render_template('display_status_B.html', data=laData['DATOS'], num_rows=len(laData['DATOS']),
    return render_template('./display_status_B.html', data=laData['DATOS'], num_rows=len(laData['DATOS']),
                           textbox_names=['textbox_' + str(i) for i in range(len(laData['DATOS']))],
                           checkbox_names=['checkbox_' + str(i) for i in range(len(laData['DATOS']))])

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form.get('username')
        password = request.form.get('password')
        # Add your authentication code here. If authentication is successful, redirect to index page
        return redirect(url_for('display_table'))  # Assuming 'display_table' is the function of your main route '/'
    return render_template('login.html')


if __name__ == '__main__':
    if thread is None:
        thread = socketio.start_background_task(send_data_to_client)
    socketio.run(app, host='localhost', port=8000,allow_unsafe_werkzeug=True,debug=True)